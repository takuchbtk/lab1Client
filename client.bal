import ballerina/io;
import ballerina/http;

type course record {
};
public function main() returns error? {

    http:Client client1 = check new http:Client("http://localhost:8080/courses");
    course[] allCourses = check client1 -> get("/all");
    foreach var  i in allCourses {
        if(i["code"] == "DSA612"){
            io:println(i);
        } 
    }
}